import { Alert } from 'react-native';
import * as env from '../constants/env';

const url = env.API_URL;
const header = env.API_HEADER;


export function getBrandSubmission(id) {
  return (dispatch,getState) => {
    dispatch({ type: 'FETCH_BRAND_SUBMISSION_LIST_FIRST'});
    const token = getState().authenticatedUser.result.token;
    header.Authorization = 'Bearer '+token;
    return fetch(`${url}/brand/submission?brand_id=${id}`, {
      method: 'GET',
      headers: header,
    })
      .then(response => response.json())
      .then((response) => {
        console.log(response,response.data)
        if(response.success){
          dispatch({
            type: 'RECEIVE_BRAND_SUBMISSION_LIST',
            result: Array.isArray(response.data)?response.data:[],
            page: response.next_page_url !== null ?2: null,
          });
        }else{
          dispatch({ type: 'STOP_FETCH_BRAND_SUBMISSION_LIST'});
        }
      })
      .catch((err) => {
        console.log(err)
        dispatch({ type: 'STOP_FETCH_BRAND_SUBMISSION_LIST'});
        Alert.alert('Connection Error', err.toString());
      });
  };
}

export function getBrandSubmissionPage(id) {
  return (dispatch,getState) => {
    const page = getState().brand_submission_list.page;
    dispatch({ type: 'FETCH_BRAND_SUBMISSION_LIST'});

    return fetch(`${url}/brand/submission?page=${page}&brand_id=${id}`, {
      method: 'GET',
      headers: header,
    })
      .then(response => response.json())
      .then((response) => {
        if(response.success){
          dispatch({
            type: 'RECEIVE_BRAND_SUBMISSION_LIST_PAGE',
            result: response.data,
            page: response.next_page_url !== null ?page+1: null,
          });
        }
      })
      .catch((err) => {
        dispatch({ type: 'STOP_FETCH_BRAND_SUBMISSION_LIST'});
        Alert.alert('Connection Error', err.toString());
      });
  };
}

export function removeOrderAllVoucher() {
  return dispatch => dispatch({ type: 'REMOVE_ALL_BRAND_SUBMISSION_LIST'});
}
