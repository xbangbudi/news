import { Alert } from 'react-native';
import * as env from '../constants/env';

const url = env.API_URL;
const header = env.API_HEADER;
import { httpClient as client } from './client';

export function submitCoomentPost(body) {
  return (dispatch,getState) => {
    const token = getState().authenticatedUser.result.token;
    dispatch({ type: 'SUBMIT_COMMENTS_POST' });
    console.log(body)
    return client
            .post(`/simpan-comment?api_token=${token}`,body, {
                headers: {}
            })
            .then((res) => {
              console.log(res)
              dispatch({
                type: 'RECEIVE_SUBMIT_COMMENTS_POST',
                result: response,
              });
            })
            .catch((err) => {
              console.log(err)
              dispatch({ type: 'STOP_COMMENTS_POST' });
            });
  };
}
