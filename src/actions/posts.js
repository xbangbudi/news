import { Alert } from 'react-native';
import * as env from '../constants/env';

const url = env.API_URL;
const header = env.API_HEADER;


export function getPost() {
  return (dispatch,getState) => {
    dispatch({ type: 'FETCH_POSTS_FIRST' });
    return fetch(`${url}/posts`, {
      method: 'GET',
      headers: header,
    })
      .then(response => response.json())
      .then((response) => {
        console.log('ckckckckc',response)
        if(response.data){
          dispatch({
            type: 'RECEIVE_POSTS',
            result: Array.isArray(response.data)?response.data:[],
            page: 2,
            total: response.meta.total,
          });
        }else{
          dispatch({ type: 'STOP_FETCH_POSTS' });
        }
      })
      .catch((err) => {
        console.log(err)
        dispatch({ type: 'STOP_FETCH_POSTS' });
        Alert.alert('Connection Error', err.toString());
      });
  };
}


export function getPostPage() {
  return (dispatch,getState) => {
    const page = getState().posts.page;
    dispatch({ type: 'FETCH_POSTS' });
    return fetch(`${url}/posts?page=${page}`, {
      method: 'GET',
      headers: header,
    })
      .then(response => response.json())
      .then((response) => {
        console.log('lzlzlzlz',response)
        if(response.data){
          dispatch({
            type: 'RECEIVE_POSTS_PAGE',
            result: Array.isArray(response.data)?response.data:[],
            page: page+1,
          });
        }else{
          dispatch({ type: 'STOP_FETCH_POSTS' });
        }
      })
      .catch((err) => {
        dispatch({ type: 'STOP_FETCH_POSTS' });
        Alert.alert('Connection Error', err.toString());
      });
  };
}

export function getPostCategory(category_id) {
  return (dispatch,getState) => {
    dispatch({ type: 'FETCH_POSTS_FIRST' });
    return fetch(`${url}/posts?categoryId=${category_id}`, {
      method: 'GET',
      headers: header,
    })
      .then(response => response.json())
      .then((response) => {
        console.log('GET POST CATEGORY',response)
        if(response.data){
          dispatch({
            type: 'RECEIVE_POSTS_CATEGORY',
            result: Array.isArray(response.data)?response.data:[],
            page: 2,
            total: response.meta.total,
          });
        }else{
          dispatch({ type: 'STOP_FETCH_POSTS' });
        }
      })
      .catch((err) => {
        console.log(err)
        dispatch({ type: 'STOP_FETCH_POSTS' });
        Alert.alert('Connection Error', err.toString());
      });
  };
}
