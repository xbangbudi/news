import { Alert } from 'react-native';
import * as env from '../constants/env';

const url = env.API_URL;
const header = env.API_HEADER;

export function submitRegister(body) {
  return (dispatch) => {
    dispatch({ type: 'SUBMIT_REGISTER' });

    return fetch(`${url}/register`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: header,
    })
      .then(response => response.json())
      .then((response) => {
        dispatch({
          type: 'RECEIVE_SUBMIT_REGISTER',
          result: response,
        });
      })
      .catch((err) => {
        dispatch({ type: 'STOP_REGISTER' });
        Alert.alert('Connection Error', err.toString());
      });
  };
}
