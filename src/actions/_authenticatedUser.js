import { Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { APP_KEY } from '../constants/env';
import { languages } from '../constants/languages'

const asyncKey = `@${APP_KEY}:authenticatedUser`;


async function setData(data) {
  try {
    await AsyncStorage.setItem(asyncKey, JSON.stringify(data));
  } catch (error) {
    Alert.alert('Error Saving Data', error.toString());
  }
}

async function loadData() {
  try {
    const data = await AsyncStorage.getItem(asyncKey);
    if (data !== null) {
      const userdata = JSON.parse(data);
      return userdata;
    }
    return {};
  } catch (error) {
    Alert.alert('Error Retrieving Data', error.toString());
  }
  return {};
}

async function removeData() {
  try {
    await AsyncStorage.removeItem(asyncKey);
  } catch (error) {
    Alert.alert('Error Removing Data', error.toString());
  }
}


export function setAuthenticatedUser(data) {

  return dispatch => setData(data)
    .then(() => {
      dispatch({
        type: 'SET_AUTHENTICATED_USER',
        result: data,
      });
    });
}

export function retrieveAuthenticatedUser() {

  return dispatch => loadData()
    .then((res) => {

      dispatch({
        type: 'RETRIEVE_AUTHENTICATED_USER',
        result: res,
      });
      if(res.lang == 'id'){
        dispatch({
          type: 'SET_LANG',
          payload: languages.indo,
        });
      }else{
        dispatch({
          type: 'SET_LANG',
          payload: languages.english,
        });
      }
    });
}


export function removeAuthenticatedUser() {
  return dispatch => removeData()
    .then(() => {
      dispatch({type: 'EMPTY_AUTHENTICATED_USER'});
    });
}
