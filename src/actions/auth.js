import { Alert } from 'react-native';
import * as env from '../constants/env';

const url = env.API_URL;
const header = env.API_HEADER;

export function submitLogin(body) {
  return (dispatch) => {
    dispatch({ type: 'SUBMIT_LOGIN' });
    return fetch(`${url}/authenticate`, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: header,
    })
      .then(response => response.json())
      .then((response) => {
        dispatch({
          type: 'RECEIVE_SUBMIT_LOGIN',
          result: response,
        });
      })
      .catch((err) => {
        console.log(err)
        dispatch({ type: 'STOP_LOGIN' });
      });
  };
}
