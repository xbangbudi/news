import axios from 'axios';
import * as env from '../constants/env';
const url = env.API_URL;

const config = {
  baseURL: url,
};

const httpClient = axios.create(config);

export {httpClient};
