import { Alert } from 'react-native';
import * as env from '../constants/env';

const url = env.API_URL;

export function getCategory(slug) {
    return (dispatch,getState) => {
      dispatch({ type: 'LOADING_FETCHING_CATEGORY' });
      console.log(`${url}/category`)
      return fetch(`${url}/category`)
        .then(response => response.json())
        .then((response) => {
          console.log('cek kategory',Array.isArray(response))
          if(response){
            dispatch({
              type: 'RECEIVE_CATEGORY_LIST',
              result: Array.isArray(response)?response:[],
            });
          }else{
            dispatch({ type: 'STOP_FETCH_CATEGORY_LIST' });
          }
        })
        .catch((err) => {
          console.log(err)
          dispatch({ type: 'STOP_FETCH_CATEGORY_LIST' });
          Alert.alert('Connection Error', err.toString());
        });
    };
  }