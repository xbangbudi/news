import { Alert } from 'react-native';
import * as env from '../constants/env';

const url = env.API_URL;
const header = env.API_HEADER;


export function getComments(slug) {
  return (dispatch,getState) => {
    dispatch({ type: 'FETCH_COMMENTS_LIST_FIRST' });
    console.log(`${url}/posts/${slug}/comments`)
    return fetch(`${url}/posts/${slug}/comments`)
      .then(response => response.json())
      .then((response) => {
        console.log('ckckckckc',response)
        if(response.data){
          dispatch({
            type: 'RECEIVE_COMMENTS_LIST',
            result: Array.isArray(response.data)?response.data:[],
            page: 2,
            total: response.meta.total,
          });
        }else{
          dispatch({ type: 'STOP_FETCH_COMMENTS_LIST' });
        }
      })
      .catch((err) => {
        console.log(err)
        dispatch({ type: 'STOP_FETCH_COMMENTS_LIST' });
        Alert.alert('Connection Error', err.toString());
      });
  };
}


export function getCommentsPage(slug) {
  return (dispatch,getState) => {
    const page = getState().comments_list.page;
    dispatch({ type: 'FETCH_COMMENTS_LIST' });
    return fetch(`${url}/posts/${slug}/comments?page=${page}`)
      .then(response => response.json())
      .then((response) => {
        console.log('lzlzlzlz',response)
        if(response.data){
          dispatch({
            type: 'RECEIVE_COMMENTS_LIST_PAGE',
            result: Array.isArray(response.data)?response.data:[],
            page: page+1,
          });
        }else{
          dispatch({ type: 'STOP_FETCH_COMMENTS_LIST' });
        }
      })
      .catch((err) => {
        dispatch({ type: 'STOP_FETCH_COMMENTS_LIST' });
        Alert.alert('Connection Error', err.toString());
      });
  };
}
