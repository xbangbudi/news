import { combineReducers } from 'redux';

import authenticatedUser from './_authenticatedUser';
import auth from './auth';
import register from './register';
import posts from './posts';
import comments_list from './comments_list';
import comments_post from './comments_post';
import category from './category';

export default combineReducers({
  authenticatedUser,
  auth,
  register,
  posts,
  comments_list,
  comments_post,
  category,
});
