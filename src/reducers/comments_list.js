const initialState = {
  isFetching: false,
  isFetching_first: false,
  result: [],
  page: 1,
  total: 0,
  correlation_id: '',
  areas: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case 'FETCH_COMMENTS_LIST_FIRST':
      return Object.assign({}, state, {
        isFetching_first: true,
        isFetching: false,
        total: 0,
        page: 1,
      });

    case 'FETCH_COMMENTS_LIST':
      return Object.assign({}, state, {
        isFetching: true,
      });

    case 'RECEIVE_COMMENTS_LIST':
      return Object.assign({}, state, {
        isFetching_first: false,
        isFetching: false,
        result: action.result,
        page: action.page,
        total: action.total
      });

    case 'RECEIVE_COMMENTS_LIST_PAGE':
      return Object.assign({}, state, {
        isFetching: false,
        isFetching_first: false,
        result: [ ...state.result, ...action.result ] ,
        page: action.page
      });

    case 'STOP_FETCH_COMMENTS_LIST':
      return Object.assign({}, state, {
        isFetching: false,
        isFetching_first: false,
      });

    case 'SET_HOTEL_CORRELATION':
      return Object.assign({}, state, {
        correlation_id: action.correlation_id,
        areas: action.areas,
      });

    case 'REMOVE_ALL_COMMENTS_LIST':
      return Object.assign({}, initialState);

    default:
      return state;
  }
}
