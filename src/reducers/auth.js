const initialState = {
  isSubmitting: false,
  submitResult: {},
};

export default function (state = initialState, action) {
  switch (action.type) {
    case 'SUBMIT_LOGIN':
      return Object.assign({}, state, {
        isSubmitting: true,
      });

    case 'RECEIVE_SUBMIT_LOGIN':
      return Object.assign({}, state, {
        isSubmitting: false,
        submitResult: action.result,
      });
    case 'STOP_LOGIN':
      return Object.assign({}, state, {
        isSubmitting: false,
      });

    case 'REMOVE_ALL_STORE_DATA':
      return Object.assign({}, initialState);

    default:
      return state;
  }
}
