const initialState = {
    result: [],
    getCategorySuccess: false,
    getCategoryError: false,
    getCategoryLoading: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case 'LOADING_FETCHING_CATEGORY':
            return Object.assign({}, state, {
                getCategoryLoading: true,
                result: [],
                getCategorySuccess: false,
                getCategoryError: false,
            });

        case 'RECEIVE_CATEGORY_LIST':
            return Object.assign({}, state, {
                getCategoryLoading: false,
                getCategorySuccess: true,
                result: action.result,
            });

        default:
            return state;
    }
}