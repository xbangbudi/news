const initialState = {
  result: {},
  isLoding:true
};

export default function (state = initialState, action) {
  switch (action.type) {
    case 'SET_AUTHENTICATED_LODING':
      return Object.assign({}, state, {
        isLoding: true,
      });
    case 'SET_AUTHENTICATED_USER':
    case 'RETRIEVE_AUTHENTICATED_USER':
      return Object.assign({}, state, {
        isLoding: false,
        result: action.result,
      });

    case 'EMPTY_AUTHENTICATED_USER':
    case 'REMOVE_ALL_STORE_DATA':
    return Object.assign({}, state, {
      isLoding: false,
      result: {},
    });

    default:
      return state;
  }
}
