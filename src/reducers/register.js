const initialState = {
  isSubmitting: false,
  submitResult: {},
};

export default function (state = initialState, action) {
  switch (action.type) {
    case 'SUBMIT_REGISTER':
      return Object.assign({}, state, {
        isSubmitting: true,
      });
    case 'STOP_REGISTER':
      return Object.assign({}, state, {
        isSubmitting: false,
      });

    case 'RECEIVE_SUBMIT_REGISTER':
      return Object.assign({}, state, {
        isSubmitting: false,
        submitResult: action.result,
      });

    case 'REMOVE_ALL_STORE_DATA_REGISTER':
      return Object.assign({}, initialState);

    default:
      return state;
  }
}
