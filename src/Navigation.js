import React from 'react';
//react

import {
  Platform,TouchableOpacity,Linking
} from 'react-native';
import Octicons from 'react-native-vector-icons/Octicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { View, Image, Text } from 'react-native';
import { enableScreens } from 'react-native-screens';
import { createStackNavigator, TransitionPresets, CardStyleInterpolators } from "@react-navigation/stack";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import styles, { colors } from './screen/styles/index.style';
import { NavigationContainer } from '@react-navigation/native';
//redux
import { useSelector } from "react-redux";
//screen
import Splash from './screen/_Splash'
//auth
import Login_enterpriser from './screen/_Auth/Login_enterpriser'
import Register from './screen/_Auth/Register'
//home
import Home from './screen/_Home'
import Portal from './screen/_Home'
import NewsDetail from './screen/_Home/detail'
//Profile
import Profile from './screen/_Profile'
import Category from './screen/_Category';
import NewsCategory from './screen/_Category/news_category';
enableScreens();


function MyTabBar({ state, descriptors, navigation }) {
  const authenticatedUser = useSelector(state => state.authenticatedUser.result);

  return (
    <View style={{ flexDirection: 'row',backgroundColor:colors.white,elevation:5}}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const name = options.tabBarIconName
        const type = options.type
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
          });
          if (!isFocused && !event.defaultPrevented) {
            if(route.name == 'Portal'){
              Linking.openURL('http://portal.potensi-utama.ac.id/')
            }else{
              navigation.navigate(route.name);
            }
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            key={index}
            accessibilityRole="button"
            accessibilityState={isFocused ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={[styles.center,{ flex: 1,paddingVertical:7}]}
          >
            {type == 'Octicons' && (
              <Octicons name={name} size={23} color={isFocused?colors.pink:'#9e9e9e'} />
            )}

            {type == 'AntDesign' && (
              <AntDesign name={name} size={24} color={isFocused?colors.pink:'#9e9e9e'} />
            )}

            <Text style={[styles.textBold,styles.sub_small,{color: isFocused?colors.pink:'#9e9e9e',textAlign:'center',marginTop:3}]}>{label}</Text>

          </TouchableOpacity>
        );
      })}
    </View>
  );
}

const Tab = createBottomTabNavigator();
function TabScreen() {
  const authenticatedUser = useSelector(state => state.authenticatedUser.result);
  const insets = useSafeAreaInsets();
    return (
      <Tab.Navigator
        backBehavior={'initialRoute'}
        initialRouteName={'Home'}
        tabBarOptions={{
            style: {
                margin: 0,
                height: 60+insets.bottom,
                color:'#9e9e9e'
            },
            labelStyle: {
                fontSize: 12,
                marginBottom: 10,
                fontFamily: Platform.OS == "ios" ? 'AvenirLTStd-Medium' :'Avenir_Medium'
            },
            activeTintColor: colors.pink,
        }}
        tabBar={props => <MyTabBar {...props} />}
        >
        <Tab.Screen
          name="Home"
          component={Home}
          options={{
            tabBarLabel: 'Home',
            tabBarIcon: ({color}) => (<Icon name="home" size={20} color={color} /> ),
            tabBarIconName: 'home',
            type: 'Octicons',
          }}
        />

        <Tab.Screen name="Portal" component={Portal} options={{
          tabBarIcon: ({ focused, color, size }) => (
            <View style={{width:60,height:60,borderRadius:50,backgroundColor:'#fff',justifyContent: 'center',alignItems: 'center',marginBottom:20}}>
              <View style={{width:45,height:45,borderRadius:50,backgroundColor:colors.pink,justifyContent: 'center',alignItems: 'center'}}>
                <AntDesign name="windows" size={20} color={color} />
              </View>
            </View>
          ),
          tabBarIconName: 'windows',
          type: 'AntDesign',
        }}/>
        <Tab.Screen
          name="Category"
          component={Category}
          options={{
            tabBarLabel: 'Category',
            tabBarIcon: ({color}) => (<Icon name="book" size={20} color={color} /> ),
            tabBarIconName: 'book',
            type: 'Octicons',
          }}
        />
        <Tab.Screen
          name="Profile"
          component={Profile}
          options={{
            tabBarLabel: 'Profile',
            tabBarIcon: ({color}) => (<Icon name="person" size={20} color={color} /> ),
            tabBarIconName: 'person',
            type: 'Octicons',
          }}
        />
      </Tab.Navigator>
    )

}


const AppStack = createStackNavigator();
const AppStackScreen = () => (
  <AppStack.Navigator sceneContainerStyle={{ backgroundColor: 'black' }} screenOptions={{
    gestureEnabled: true,
    gestureDirection: "horizontal",
    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
    cardStyle: { backgroundColor: 'transparent' },
  }}>

    <AppStack.Screen name="Tabs" component={TabScreen} options={{ headerShown: false, }} />
  </AppStack.Navigator>
);

const RootStack = createStackNavigator();

export default function App() {
  const authenticatedUser = useSelector(state => state.authenticatedUser.result);
  const loding = useSelector(state => state.authenticatedUser.isLoding);

  if (loding) {
    return <Splash />;
  }

  return (
    <NavigationContainer>
      <RootStack.Navigator headerMode="none">
        <RootStack.Screen name="App" component={AppStackScreen} options={{ animationEnabled: false }}/>
        <RootStack.Screen name="Register" component={Register} options={{ headerShown: false, }} />
        <RootStack.Screen name="Login_enterpriser" component={Login_enterpriser} options={{ headerShown: false, }} />
        <RootStack.Screen name="NewsDetail" component={NewsDetail} options={{ headerShown: false, }} />
        <RootStack.Screen name="NewsCategory" component={NewsCategory} options={{ headerShown: false, }} />
      </RootStack.Navigator>
    </NavigationContainer>
  );
}
