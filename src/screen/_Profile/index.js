import React, { Component } from 'react';
import { View, Text ,StatusBar,SafeAreaView,TouchableOpacity,ScrollView,Image,ActivityIndicator,Linking,RefreshControl,Modal,FlatList,Alert,Dimensions} from 'react-native';
import styles, { colors,itemWidthslider,itemWidthslidercontener } from '../styles/index.style';
import Input from '../Components/Input'
import AutoHeightImage from 'react-native-auto-height-image';
import Button from '../Components/Button'
import AntDesign from 'react-native-vector-icons/AntDesign';

const {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT,
} = Dimensions.get('window');

//redux
import { connect } from 'react-redux';
import { removeAuthenticatedUser } from '../../actions/_authenticatedUser';


import * as env from '../../constants/env';
const OS = Platform.OS;
const version_app = env.VERSION_APP[OS].code

class Home extends Component {


  render() {
    const {authenticatedUser} = this.props
    return (
      <SafeAreaView style={styles.flex1}>
      <StatusBar translucent={true} backgroundColor={'transparent'} barStyle={"dark-content"}/>
        <View style={{flex:1}}>
          <View style={[styles.view_header_not_shadow_stuck_shadow,styles.flexRow,{backgroundColor:colors.white,paddingHorizontal:20}]}>
            <View style={{ flex:1,justifyContent: 'center',alignItems: 'center'}}>
              <Text style={[styles.textMedium,styles.medium_small,{color: colors.black,textAlign:'left'}]}>Profile</Text>
            </View>
          </View>
          {authenticatedUser.token?(
            <ScrollView style={{flex:1}}>

            <View style={[{backgroundColor:colors.white,marginHorizontal:20,borderRadius:15,padding:5,marginTop:20,shadowColor: '#969696',shadowOffset: { width: 0, height: 2 },shadowOpacity: 0.5,shadowRadius: 2,elevation:7,marginBottom:10}]}>
                <View style={[styles.flexRow,{borderRadius:5,padding:7,alignItems: 'center'}]}>
                  <Image
                    style={{height:60,width:60,borderRadius:100}}
                    source={{uri:'https://images.pexels.com/photos/2379004/pexels-photo-2379004.jpeg'}}
                    resizeMode='cover'
                  />
                  <View style={[styles.flex1,{marginLeft:15,justifyContent: 'center'}]}>
                    <View style={[styles.flexRow,{marginLeft:0,alignItems: 'center',borderColor:'#f0f0f0',borderBottomWidth:0.9,paddingVertical:8}]}>
                      <Text style={[styles.textMedium,styles.normal,{color: colors.black,textAlign:'left',flex:1}]}>{authenticatedUser.user && authenticatedUser.user.name}</Text>
                    </View>
                    <View style={[styles.flexRow,{marginLeft:0,alignItems: 'center',paddingVertical:8}]}>
                      <Text style={[styles.textNormal,styles.normal,{color: colors.black,textAlign:'left',flex:7}]}>{authenticatedUser.user && authenticatedUser.user.email}</Text>
                    </View>
                  </View>
                </View>
              </View>



              <View style={[{backgroundColor:colors.white,marginHorizontal:20,borderRadius:15,padding:5,marginTop:10,shadowColor: '#969696',shadowOffset: { width: 0, height: 2 },shadowOpacity: 0.5,shadowRadius: 2,elevation:7,marginBottom:30}]}>
                <TouchableOpacity  onPress={() => this.props.removeAuthenticatedUser()} style={[styles.flex1,{marginHorizontal:10,justifyContent: 'center'}]}>
                  <View style={[styles.flexRow,{marginLeft:0,alignItems: 'center',paddingVertical:8}]}>
                    <Text style={[styles.textMedium,styles.normal,{color: colors.black,textAlign:'left'}]}>Sign Out</Text>
                  </View>
                </TouchableOpacity>
              </View>

            </ScrollView>
          ):(
            <View style={{justifyContent: 'center',flex:1}}>
              <Button onPress={() => this.props.navigation.navigate('Login_enterpriser')} name={'LOGIN'} style={{marginHorizontal:20}}/>
            </View>
          )}

        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  authenticatedUser: state.authenticatedUser.result,
});

export default connect(mapStateToProps,{
  removeAuthenticatedUser
})(Home);

const _styles = {
  imageBackground: {
    ...styles.flex1,
    resizeMode: "strech"
  }
}
