import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity} from 'react-native';
import styles, { colors } from '../styles/index.style';
import { connect } from 'react-redux';
import { removeAuthenticatedUser } from '../../actions/_authenticatedUser';

class HeaderHome extends Component {

    handleLogOut(){
      const { removeAuthenticatedUser } = this.props;
      removeAuthenticatedUser()
    }

    render () {
        const { title } = this.props;
        return (
          <View style={styles.header}>
            {
              // <View style={{ flex:1,justifyContent: 'center'}}>
              //   <View style={{width:30,height:30,borderRadius:50,backgroundColor:'#fff'}}/>
              // </View>
            }

            <View style={{ flex:9,justifyContent: 'center'}}>
              <Text style={[styles.text_SemiBold,{color: '#fff',textAlign:'center',paddingVertical:10,paddingHorizontal:5,fontSize:20}]}>{title}</Text>
            </View>

            <TouchableOpacity onPress={() => this.handleLogOut()} style={{ flex:1,justifyContent: 'center'}}>
            </TouchableOpacity>

          </View>
        );
    }
}
const mapStateToProps = state => ({
  authenticatedUser: state.authenticatedUser.result,
});

export default connect(mapStateToProps,{removeAuthenticatedUser})(HeaderHome);
