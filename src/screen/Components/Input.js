import React from 'react';
import {StyleSheet,TextInput,View,Platform} from 'react-native';
import styles, { colors } from '../styles/index.style';
import AntDesign from 'react-native-vector-icons/AntDesign';
const IS_ANDROID = Platform.OS === 'android';
function Input(props) {
  return (
    <View style={[props.style_input,_styles.containerStyle,{paddingVertical:props.search?5:8}]}>
      {props.search && (
        <View style={[styles.center,{marginLeft:10}]}>
          <AntDesign name="search1" size={20} color={'#9e9e9e'} />
        </View>
      )}
      <View style={{flex:1}}>
        <TextInput
          style={[_styles.inputStyle,{color:props.white?'#fff':colors.input}]}
          underlineColorAndroid="transparent"
          autoCapitalize="none"
          {...props}
          placeholderTextColor={colors.placeholder}
        />
      </View>
    </View>
  );

}

const _styles = {
  containerStyle: {

    borderRadius:7.4,
    backgroundColor:'#f5f5f8',
    ...styles.flexRow,
    // shadowColor: '#969696',shadowOffset: { width: 0, height: 2 },shadowOpacity: 0.5,shadowRadius: 2,elevation:3
  },
  inputStyle: {

    ...styles.textNormal,
    ...styles.normal,
    paddingHorizontal:15,
    paddingVertical:IS_ANDROID?0:8,


  }
}

export default Input;
