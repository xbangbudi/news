import React from 'react';
import {StyleSheet,TouchableOpacity,Text,View,ActivityIndicator} from 'react-native';
import styles, { colors } from '../styles/index.style';

function Button(props) {
  return (
    <View style={props.style}>
      {props.loading?(
        <View
          activeOpacity={0.7}
          {...props}
          style={_styles.containerStyle}
          >
          <ActivityIndicator size="small" color="#fff" style={_styles.textStyle}/>


        </View>
      ):(
        <TouchableOpacity
          activeOpacity={0.7}
          {...props}
          style={[_styles.containerStyle,props.backgroundColor && { backgroundColor:props.backgroundColor}]}
          >

          <Text style={[_styles.textStyle,props.textColor && { color:props.textColor}]}>{props.name}</Text>
        </TouchableOpacity>
      )}

    </View>
  );

}

const _styles = {
  containerStyle: {
    justifyContent: 'center',
    borderRadius:10,
    backgroundColor:colors.pink,
    shadowColor: '#969696',shadowOffset: { width: 0, height: 2 },shadowOpacity: 0.5,shadowRadius: 2,elevation:3
  },
  textStyle: {
    ...styles.textMedium,
    ...styles.normal,
    color: colors.black,
    textAlign:'center',
    paddingVertical:11,
    paddingHorizontal:5

  }
}

export default Button;
