import React, { Component } from 'react';
import { Dimensions } from "react-native";
import FastImage from "react-native-fast-image";
const { width } = Dimensions.get("window");

export default class SliderImages extends Component {
    state = {
      calcImgHeight: 0,
    };
    render () {
      const { calcImgHeight } = this.state;
      const {data} = this.props;
        return (
          <FastImage
              style={{ width: width-40, height: calcImgHeight}}
              source={{uri: data}}
              resizeMode={FastImage.resizeMode.contain}
              onLoad={evt =>
                this.setState({
                  calcImgHeight: evt.nativeEvent.height / evt.nativeEvent.width * width,
                })
              }
          />

        );
    }
}
