import React from 'react';
import Modal from 'react-native-modal';
import ImagePicker from 'react-native-image-picker';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';

function ContentPicker({
  isVisible = false,
  toogleShowContentPicker,
  mediaType = 'photo', // photo || video || mixed
  onSuccess = (response) => console.log('Content Picker: Success!'),
  maxHeight = 300,
  maxWidth = 300
}) {
  const onTakePhoto = () => {
    toogleShowContentPicker();
    ImagePicker.launchCamera(
      {
        mediaType: 'photo',
        includeBase64: false,
        maxHeight: maxHeight,
        maxWidth: maxWidth
      },
      (response) => {
        if (!response.didCancel) {
          onSuccess(response);
        }
      },
    );
  };

  const onTakeVideo = () => {
    toogleShowContentPicker();
    ImagePicker.launchCamera(
      {
        mediaType: 'video',
        includeBase64: false,
      },
      (response) => {
        if (!response.didCancel) {
          let pathArr = response.path.split('/');
          let fileName = pathArr[pathArr.length - 1];
          let fileType = fileName.split('.')[1];

          onSuccess({
            fileName: fileName,
            type: `video/${fileType}`,
            uri: response.uri,
            path: response.path,
          });
        }
      },
    );
  };

  const onChoosePhotoFromGalery = () => {
    toogleShowContentPicker();
    ImagePicker.launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: false,
      },
      (response) => {
        if (!response.didCancel) {
          onSuccess(response);
        }
      },
    );
  };

  const onChooseVideoFromGallery = () => {
    toogleShowContentPicker();
    ImagePicker.launchImageLibrary(
      {
        mediaType: 'video',
        durationLimit: 60,
        includeBase64: true,
      },
      (response) => {
        if (!response.didCancel) {
          let pathArr = response.path.split('/');
          let fileName = pathArr[pathArr.length - 1];
          let fileType = fileName.split('.')[1];

          onSuccess({
            fileName: fileName,
            type: `video/${fileType}`,
            uri: response.uri,
            path: response.path,
          });
        }
      },
    );
  };

  return (
    <Modal
      isVisible={isVisible}
      onBackdropPress={toogleShowContentPicker}
      onBackButtonPress={toogleShowContentPicker}
      useNativeDriver={true}>
      <View style={styles.contentPickerWrapper}>
        <Text style={[styles.contentPickerTitle, {color: '#009444'}]}>
          Select Content
        </Text>
        <TouchableOpacity
          style={[
            styles.contentPickerItemWrapper,
            mediaType === 'video' && {display: 'none'},
          ]}
          onPress={onTakePhoto}>
          <Text style={styles.contentPickerItem}>Take Photo...</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[
            styles.contentPickerItemWrapper,
            mediaType === 'photo' && {display: 'none'},
          ]}
          onPress={onTakeVideo}>
          <Text style={styles.contentPickerItem}>Take Video...</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[
            styles.contentPickerItemWrapper,
            mediaType === 'video' && {display: 'none'},
          ]}
          onPress={onChoosePhotoFromGalery}>
          <Text style={styles.contentPickerItem}>
            Choose Photo from Gallery...
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[
            styles.contentPickerItemWrapper,
            mediaType === 'photo' && {display: 'none'},
          ]}
          onPress={onChooseVideoFromGallery}>
          <Text style={styles.contentPickerItem}>
            Choose Video from Gallery...
          </Text>
        </TouchableOpacity>
        <Text
          style={styles.contentPickerCancel}
          onPress={toogleShowContentPicker}>
          CANCEL
        </Text>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  contentPickerWrapper: {
    backgroundColor:'#fff',
    paddingVertical: 20,
    borderRadius: 10,
  },
  contentPickerTitle: {
    fontFamily:'Montserrat-Medium',
    marginLeft: 20,
    marginBottom: 15,
  },
  contentPickerItemWrapper: {
    backgroundColor: '#5A5457',
    marginBottom: 5,
  },
  contentPickerItem: {
    fontFamily:'Montserrat-Medium',
    backgroundColor: '#fff',
    paddingVertical: 15,
    paddingHorizontal: 20,
  },
  contentPickerCancel: {
    fontFamily:'Montserrat-Medium',
    alignSelf: 'flex-end',
    marginTop: 10,
    marginRight: 20,
  },
});

export default ContentPicker;
