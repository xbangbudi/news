import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import styles, { colors} from '../styles/index.style';
import AntDesign from 'react-native-vector-icons/AntDesign';

const SellCarItem = ({item,onPress}) => {
  try{

    return (
      <TouchableOpacity onPress={() => onPress()}  style={[{backgroundColor:colors.white,marginHorizontal:20,borderRadius:15,padding:5,marginTop:5,shadowColor: '#969696',shadowOffset: { width: 0, height: 2 },shadowOpacity: 0.5,shadowRadius: 2,elevation:7,marginBottom:10,}]}>
          <View style={[styles.flexRow,{borderRadius:5,padding:7,alignItems: 'center'}]}>
          <Image
            style={{height:60,width:60,borderRadius:10}}
            source={{uri:item.uri}}
            resizeMode='cover'
          />
          <View style={[styles.flex1,{marginLeft:10,justifyContent: 'center'}]}>
            <View style={[styles.flexRow,{marginLeft:0,alignItems: 'center'}]}>
              <Text style={[styles.textMedium,styles.small_13,{color: colors.black_soft,textAlign:'left',flex:1}]}>{item.name}</Text>
              <AntDesign name="right" size={14} color={colors.black} />
            </View>

            <Text style={[styles.textMedium,styles.sub_small,{color: colors.pink,textAlign:'left',marginTop:0}]}>ID : 6236723</Text>
            <Text style={[styles.textMedium,styles.sub_small,{color: colors.gray_soft,textAlign:'left',marginTop:0}]}>Skin type : {item.type}</Text>
            <Text style={[styles.textMedium,styles.sub_small,{color: colors.gray_soft,textAlign:'left',marginTop:0}]}>{item.date}</Text>
          </View>
          </View>
        </TouchableOpacity>
    )
  }catch(err){
    console.log('ERROR renderItem ', err.message)
    return null
  }
}

const _styles = {
  containerStyle: {
    justifyContent: 'center',
    borderRadius:10,
    backgroundColor:colors.pink,
    shadowColor: '#969696',shadowOffset: { width: 0, height: 2 },shadowOpacity: 0.5,shadowRadius: 2,elevation:3,
    marginTop:5
  },
  image_product: {
    width: '100%',
    marginTop:5,
  	height: 150,
  },
  textStyle: {
    ...styles.textMedium,
    ...styles.small,
    color: colors.black,
    textAlign:'center',
    paddingVertical:7,
    paddingHorizontal:5

  }
}

export default SellCarItem;
