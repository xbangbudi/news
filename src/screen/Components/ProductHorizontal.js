import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import styles, { colors} from '../styles/index.style';

const SellCarItem = ({data,onPress}) => {
  try{

    return (
      <View activeOpacity={0.7}  style={styles.item_product_horizontal}>
        <View style={{backgroundColor:'#fff',borderRadius:3,flex:1,borderRadius:10}}>
        <Image
          style={_styles.image_product}
          source={{uri:'https://id.bskin.com/media/wysiwyg/ss-cream-shadow.jpg'}}
          resizeMode='contain'
        />
        <View style={{borderTopWidth:1,borderColor:'rgb(246,247,251)',padding:10}}>
          <Text style={[styles.textMedium,styles.normal,{color: colors.gray,textAlign:'left',lineHeight: 20}]}>V1 BSKIN Vita Advanced - Soft Bead Cleanser (100ml)</Text>
          <TouchableOpacity
            activeOpacity={0.7}
            style={_styles.containerStyle}
            onPress={() => onPress()}
            >
            <Text style={_styles.textStyle}>See More</Text>
          </TouchableOpacity>
        </View>



        </View>
      </View>
    )
  }catch(err){
    console.log('ERROR renderItem ', err.message)
    return null
  }
}

const _styles = {
  containerStyle: {
    justifyContent: 'center',
    borderRadius:5,
    backgroundColor:colors.pink,
    marginTop:5
  },
  image_product: {
    width: '100%',
    marginTop:5,
  	height: 150,
  },
  textStyle: {
    ...styles.textMedium,
    ...styles.small,
    color: colors.black,
    textAlign:'center',
    paddingVertical:10,
    paddingHorizontal:5
  }
}

export default SellCarItem;
