import React, { Component } from 'react';
import { View,Image,SafeAreaView,StatusBar,Text} from 'react-native';
import styles, { colors } from '../styles/index.style';
//redux
import { connect } from 'react-redux';
import { retrieveAuthenticatedUser } from '../../actions/_authenticatedUser';

class Splash extends Component {
  componentDidMount = () => {
    const {
      retrieveAuthenticatedUser,
    } = this.props;
    setTimeout(()=>{
      retrieveAuthenticatedUser()
    }, 3000);
  }

  render() {
    return (
      <SafeAreaView style={styles.flex1}>
        <StatusBar translucent={true} backgroundColor={'transparent'} barStyle={'dark-content'}/>
        <View style={_styles.imageBackground}>
        <Image
          style={{height:150,width:'100%',marginTop:20}}
          source={require('../img/app/logo-universitas-potensi-utama-1024x1024.jpg')}
          resizeMode='contain'
        />
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
    authenticatedUser: state.authenticatedUser.result,
});

export default connect(mapStateToProps,{
  retrieveAuthenticatedUser
})(Splash);

const _styles = {
  imageBackground: {
    ...styles.flex1,
    resizeMode: "strech",
    justifyContent: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:colors.white
  }
}
