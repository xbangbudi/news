import { StyleSheet,Dimensions,Platform,StatusBar,PixelRatio } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
const {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT,
} = Dimensions.get('window');

const scale = SCREEN_WIDTH / 320;

const IS_ANDROID = Platform.OS === 'android';

export function normalize(size) {
  const newSize = size * scale
  if (Platform.OS === 'ios') {
    return Math.round(PixelRatio.roundToNearestPixel(newSize))
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
  }
}

export const colors = {
    white: '#ffffff',
    pink: '#f8c0b0',
    pink_soft: '#fff4ea',
    pink_hard: '#dc856c',

    red: '#d8222a',
    gray: '#4c4c4c',
    gray_soft: '#929292',
    gray_light: '#f5f5f8',

    black: '#000000',
    black_soft: '#373535',
    green: '#00ff7c',
    background: 'rgb(246,247,251)',
    background_red: '#FF3333',
    background_grey: '#F0F0F0',
    red:'#d8222a',
    border: '#7d767e',
    placeholder: '#7d767e',
    input: '#1f1f1f'

};

export const font = {
    Medium: 'Montserrat-Medium',
};

export default StyleSheet.create({

    mini: {
      fontSize: 9,
    },
    sub_small: {
      fontSize: 10,
    },
    small: {
      fontSize: 12,
    },
    small_13: {
      fontSize: 13,
    },
    normal: {
      fontSize: 15,
    },
    normal_up: {
      fontSize: 16,
    },
    medium_small: {
      fontSize: 17,
    },
    medium: {
      fontSize: 17,
    },
    large: {
      fontSize: 20,
    },
    xlarge: {
      fontSize: 24,
    },

    textBold: {
      fontFamily: Platform.OS == "ios" ? 'AvenirLTStd-Medium' : 'AvenirLTStd-Medium'
    },
    textMedium: {
      fontFamily: Platform.OS == "ios" ? 'AvenirLTStd-Medium' : 'AvenirLTStd-Medium'
    },
    textNormal: {
      fontFamily: Platform.OS == "ios" ? 'AvenirLTStd-Book' :'AvenirLTStd-Book'
    },
    textItalic: {
      fontFamily: Platform.OS == "ios" ? 'AvenirLTStd-Book' : 'AvenirLTStd-Book'
    },
    header:{
      flexDirection: 'row',
      height:IS_ANDROID?(50+StatusBar.currentHeight):50,
      backgroundColor:'#000000a1',
      paddingTop:IS_ANDROID?StatusBar.currentHeight:0,
      paddingHorizontal:15,
      marginBottom:1.5
    },
    flex1: {
      flex: 1,
      backgroundColor:colors.white
    },
    flexRow: {
      flexDirection: 'row',
    },
    loadingSpinner:{
      zIndex: 1000,
      position: 'absolute',
      backgroundColor:'#3333333b',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      justifyContent: 'center',
      alignItems: 'center'
    },
    center:{
      justifyContent: 'center',
      alignItems: 'center'
    },
    view_header:{
      height:IS_ANDROID?(60+StatusBar.currentHeight):60,
      paddingTop:IS_ANDROID?StatusBar.currentHeight:0,
      backgroundColor:'#ffffff',
      shadowColor: '#969696',shadowOffset: { width: 0, height: 2 },shadowOpacity: 0.5,shadowRadius: 2,elevation:5
    },
    view_header_not_shadow:{
      height:IS_ANDROID?(70+StatusBar.currentHeight):70,
      paddingTop:IS_ANDROID?StatusBar.currentHeight+20:20,
    },
    view_header_not_shadow_stuck:{
      height:IS_ANDROID?(60+StatusBar.currentHeight):60,
      paddingTop:IS_ANDROID?StatusBar.currentHeight:0,
    },
    view_header_not_shadow_stuck_shadow:{
      height:IS_ANDROID?(60+StatusBar.currentHeight):60,
      paddingTop:IS_ANDROID?StatusBar.currentHeight:0,
      shadowColor: '#969696',shadowOffset: { width: 0, height: 2 },shadowOpacity: 0.5,shadowRadius: 2,elevation:5,
      backgroundColor:'#ffffff',
    },
    image_product: {
    	width: '100%',
      marginTop:5,
    	height: SCREEN_HEIGHT/4-20,
    },
    item_product: {
    	maxWidth: '50%',
      flex: 1,
      padding:7,
    },


    slider: {
        marginTop: 5,
        overflow: 'visible' // for custom animations
    },
    sliderContentContainer: {
        paddingVertical: 0 // for custom animation
    },

    item_product_horizontal: {
      width:SCREEN_WIDTH/2-30,
      borderRadius:8,
      marginRight: 7,
      marginLeft: 7,
      marginBottom: 7,
      flex:1,
      backgroundColor:'#fff',
      shadowColor: '#969696',shadowOffset: { width: 0, height: 2 },shadowOpacity: 0.5,shadowRadius: 2,
      elevation:3,
      marginTop:3
    },
    loadingSpinner:{
      zIndex: 1000,
  		position: 'absolute',
  		top: 0,
  		left: 0,
  		right: 0,
  		bottom: 0,
  		justifyContent: 'center',
  		alignItems: 'center',
      backgroundColor:'#3333333b',
    },
    loadingSpinner_:{
      zIndex: 1000,
  		position: 'absolute',
  		top: 0,
  		left: 0,
  		right: 0,
  		bottom: 0,
      backgroundColor:'#3333333b',
    }

});
