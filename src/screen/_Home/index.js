import React, { Component } from 'react';
import { View, Text, StatusBar, SafeAreaView, TouchableOpacity, ScrollView, Image, ActivityIndicator, Linking, RefreshControl, FlatList, Alert, ImageBackground, Dimensions } from 'react-native';
import styles, { colors, itemWidthslider, itemWidthslidercontener } from '../styles/index.style';
import Icon from 'react-native-vector-icons/Entypo';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';
import 'moment/locale/id'
moment.locale('id')

const {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT,
} = Dimensions.get('window');

//redux
import { connect } from 'react-redux';
import { getPost, getPostPage } from '../../actions/posts';

class Home extends Component {

  componentDidMount = () => {
    const unsubscribe = this.props.navigation.addListener('focus', () => {
      this.props.getPost()
    });
  }

  load_more() {
    const { dataFetching, dataPage, dataResult, dataTotal, getPostPage } = this.props;
    if (dataFetching) {
      return;
    }
    if (dataResult.length < dataTotal) {
      getPostPage()
    }
  }


  updateStatusModal() {
    const { setAuthenticatedUser } = this.props;
    var datas = {
      user: {
        name: 'Bayu Zauhari',
      },
      type: 'enterprise',
      token: '123456',
      is_modal: false
    }
    setAuthenticatedUser(datas)
  }

  onPressProductDetail(data) {
    this.props.navigation.navigate('NewsDetail', {
      data: data,
    })
  }

  clea_taghtml(data) {
    const regex = /(<([^>]+)>)/ig;
    const result = data.replace(regex, '');
    return result;
  }

  renderItem = (item) => {
    return (
      <TouchableOpacity onPress={() => this.onPressProductDetail(item)} style={[styles.flexRow, { marginHorizontal: 15, borderBottomWidth: 0.5, borderColor: '#969696', paddingVertical: 15 }]}>
        <View style={{ flex: 3, justifyContent: 'center' }}>
          <Image
            style={{ height: 70, width: '100%', borderRadius: 5 }}
            source={{ uri: item.thumb_thumbnail_url }}
            resizeMode='contain'
          />
        </View>
        <View style={{ flex: 7, justifyContent: 'center', marginLeft: 10 }}>
          <Text style={[styles.textMedium, styles.mini, { color: colors.red, textAlign: 'left' }]}>{moment(item.posted_at).format('dddd, DD MMMM YYYY')}</Text>
          <Text style={[styles.textBold, styles.small, { color: colors.black, textAlign: 'left' }]}>{item.title}</Text>
          <Text style={[styles.textNormal, styles.sub_small, { color: colors.gray_, textAlign: 'left' }]} numberOfLines={2}>{this.clea_taghtml(item.content)}</Text>
        </View>
      </TouchableOpacity>
    )
  }

  render() {
    const { authenticatedUser, dataResult, dataFetchingFirst, dataFetching } = this.props;
    return (
      <SafeAreaView style={styles.flex1}>
        <StatusBar translucent={true} backgroundColor={'transparent'} barStyle={"dark-content"} />
        <View style={{ flex: 1 }}>
          <View style={[styles.view_header_not_shadow_stuck_shadow, styles.flexRow, { backgroundColor: colors.white, paddingHorizontal: 20 }]}>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <Image
                style={{ height: 20, width: '100%' }}
                source={require('../img/app/logo-universitas-potensi-utama-1024x1024.jpg')}
                resizeMode='contain'
              />
            </View>
            <View style={{ flex: 8, justifyContent: 'center' }}>
              <Text style={[styles.textMedium, styles.normal_up, { color: colors.black, textAlign: 'left' }]}>UPU MOBILE</Text>
            </View>
            <TouchableOpacity onPress={() => {
              Linking.openURL('https://seodimedan.com')
            }} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <Icon name="globe" size={20} color="#000" />
            </TouchableOpacity>
          </View>
          {dataFetchingFirst ? (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <ActivityIndicator size="large" color={colors.red} />
            </View>
          ) : (
            <FlatList
              data={dataResult}
              renderItem={({ item }) => (
                this.renderItem(item)
              )}
              contentContainerStyle={{ paddingBottom: 70 }}
              ListEmptyComponent={
                <View style={[styles.center, { flex: 1 }]}>

                  <Text style={[styles.textBold, styles.small, { color: colors.red, textAlign: 'left', marginTop: SCREEN_HEIGHT / 2 - 70 }]}>Artikel Tidak tersedia</Text>
                  <Text style={[styles.textBold, styles.small, { color: colors.black, textAlign: 'left' }]}>Silahkan coba beberapa saat lagi</Text>
                </View>

              }
              keyExtractor={(item, index) => '_' + index}
              ListFooterComponent={
                <ActivityIndicator
                  style={[
                    { paddingTop: 15 },
                    !dataFetching && { display: 'none' },
                  ]}
                  size="large"
                  color={colors.red}
                />
              }
              onEndReached={() => this.load_more()}
              maxToRenderPerBatch={10}
              windowSize={2}
              updateCellsBatchingPeriod={10}
              initialNumToRender={6}
              onEndReachedThreshold={1}
            />
          )}


        </View>

      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  authenticatedUser: state.authenticatedUser.result,

  dataResult: state.posts.result,
  dataFetching: state.posts.isFetching,
  dataFetchingFirst: state.posts.isFetching_first,
  dataPage: state.posts.page,
  dataTotal: state.posts.total,
});

export default connect(mapStateToProps, {
  getPost, getPostPage
})(Home);

const _styles = {
  imageBackground: {
    ...styles.flex1,
    resizeMode: "strech"
  }
}
