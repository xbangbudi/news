import React, { Component } from 'react';
import { View, Text ,StatusBar,SafeAreaView,TouchableOpacity,ScrollView,Image,ActivityIndicator,Linking,RefreshControl,FlatList,Alert,TextInput,Dimensions} from 'react-native';
import styles, { colors,itemWidthslider,itemWidthslidercontener } from '../styles/index.style';
import Icon from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AutoHeightImage from 'react-native-auto-height-image';
import RenderHtml from 'react-native-render-html';
import Button from '../Components/Button'
import Toast from 'react-native-toast-message'

import moment from 'moment';
import 'moment/locale/id'
moment.locale('id')

const {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT,
} = Dimensions.get('window');


//redux
import { connect } from 'react-redux';
import { getComments,getCommentsPage } from '../../actions/comments_list';
import { submitCoomentPost } from '../../actions/comments_post';

class Home extends Component {
  state={
    comment:''
  }
  componentDidMount = () => {
    const { data } = this.props.route.params;
    this.props.getComments(data.slug)
  }




  updateStatusModal(){
    const { setAuthenticatedUser } = this.props;
    var datas = {
      user:{
        name : 'Bayu Zauhari',
      },
      type : 'enterprise',
      token:'123456',
      is_modal:false
    }
    setAuthenticatedUser(datas)
  }

  onPressProductDetail(){
    this.props.navigation.navigate('News_detail', {
      data: {},
    })
  }

  clea_taghtml(data){
    const regex = /(<([^>]+)>)/ig;
    const result = data.replace(regex, '');
    return result;
  }

  handleDone = () => {
    if(this.state.comment == ''){
      this.toas('error','Comment harus diisi')
    }else{
      const { data } = this.props.route.params;
      const body = {
        post_id:data.id,
        content:this.state.comment
      }
      this.props.submitCoomentPost(body).then(() => {
          const { result,getComments } = this.props;
          getComments(data.slug)
          this.setState({comment:''})
      });
    }
  }

  toas(type,message){
    Toast.show({
      type: type,
      visibilityTime: 2000,
      position: 'bottom',
      text1: 'NEWSLO',
      text2: message+' 👋'
    })
  }


  _renderFooter = () => {
    const { dataFetching,dataResult,dataPage,dataTotal,getCommentsPage } = this.props;
    const { data } = this.props.route.params;
    if(dataResult.length < dataTotal){
      if(dataFetching){
        return (
          <View style={{justifyContent: 'center',alignItems: 'center',marginBottom:15,marginTop:10}}>
            <View style={{flexDirection: 'row',backgroundColor:'#fff',paddingHorizontal:15,paddingVertical:10,borderRadius:8,borderColor:'#9e9e9e',borderWidth:0.5,justifyContent: 'center',alignItems: 'center'}}>
              <ActivityIndicator size="small" color="#000" style={{textAlign: 'center',marginRight:10}}/>
              <Text  style={{textAlign: 'center',color:'#9e9e9e',fontFamily:'Montserrat-Light',}}>Load More Outlet</Text>
            </View>
          </View>

        )
      }else{
        return (
          <View style={{justifyContent: 'center',alignItems: 'center',marginBottom:15,marginTop:10}}>
            <View style={{backgroundColor:'#fff',paddingHorizontal:15,paddingVertical:10,borderRadius:8,borderColor:'#9e9e9e',borderWidth:0.5}}>
              <Text onPress={() => getCommentsPage(data.slug)} style={{textAlign: 'center',color:'#9e9e9e',fontFamily:'AvenirLTStd-Medium',}}>Load More Comment</Text>
            </View>
          </View>

        )
      }
    }else{
      return null
    }
  }

  render() {
    const { authenticatedUser,dataResult,dataFetchingFirst,dataFetching,loading } = this.props;
    const { data } = this.props.route.params;
    console.log(data)

    const tagsStyles = {
      p: {
        color: '#000',
        fontFamily:'AvenirLTStd-Medium',
        fontSize: 13
      },
      span: {
        color: '#000',
        fontFamily:'AvenirLTStd-Medium',
        fontSize: 13
      },
    };

    const systemFonts = ['AvenirLTStd-Medium']
    return (
      <SafeAreaView style={styles.flex1}>
      <StatusBar translucent={true} backgroundColor={'transparent'} barStyle={"dark-content"}/>
        <View style={{flex:1}}>
        <View style={[styles.view_header_not_shadow_stuck_shadow,styles.flexRow,{backgroundColor:colors.white,paddingHorizontal:20}]}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{ flex:1,justifyContent: 'center',alignItems: 'center'}}>
            <Icon name="left" size={20} color="#000" />
          </TouchableOpacity>
          <View style={{ flex:8,justifyContent: 'center',alignItems: 'center'}}>
            <Text style={[styles.textMedium,styles.normal_up,{color: colors.black,textAlign:'left'}]}>Detail</Text>
          </View>
          <View style={{ flex:1}}/>
        </View>

        <ScrollView style={{flex:1}} keyboardShouldPersistTaps={'always'}>

          <View style={{marginHorizontal:15,marginTop:15}}>
            <Text style={[styles.textBold,styles.medium,{color: colors.black,textAlign:'left'}]}>{data.title}</Text>
            <Text style={[styles.textBold,styles.small,{color: colors.gray_,textAlign:'left'}]}>{moment(data.posted_at).format('dddd, DD MMMM YYYY')}</Text>
            <View style={{width:'30%',height:3,backgroundColor:colors.red,marginTop:5}}/>
          </View>

          <AutoHeightImage
            style={{marginVertical:10}}
            width={SCREEN_WIDTH}
            source={{uri:data.thumb_thumbnail_url}}
          />
          <View style={{marginHorizontal:20}}>
          <RenderHtml
            contentWidth={SCREEN_WIDTH-20}
            source={{html:data.content}}
            tagsStyles={tagsStyles}
            systemFonts={systemFonts}
          />
          </View>
          {authenticatedUser.token?(
            <View>
              <View style={{elevation:3,marginHorizontal:20,backgroundColor:'#fff',borderRadius:10,marginBottom:10}}>
                <TextInput
                  style={[{color:'#000',paddingHorizontal:15,paddingVertical:0,fontFamily:'AvenirLTStd-Medium'}]}
                  underlineColorAndroid="transparent"
                  autoCapitalize="none"
                  placeholder={'Comment'}
                  placeholderTextColor={'#969696'}
                  onChangeText={text => this.setState({comment:text})}
                  value={this.state.comment}
                  multiline
                  numberOfLines={7}
                />
              </View>
              <Button onPress={this.handleDone}  name={'Comment'} style={{marginVertical:10,marginHorizontal:20}} loading={loading}/>
            </View>
          ):(
            <View>
              <Text style={[styles.textMedium,styles.normal,{color: colors.black,textAlign:'left',marginHorizontal:20,marginVertical:10,backgroundColor:'#fff8dd',padding:20,borderRadius:10}]}>You must be signed in to comment.</Text>
              <Button onPress={() => this.props.navigation.navigate('Login_enterpriser')} name={'LOGIN'} style={{marginHorizontal:20,marginBottom:10}}/>
            </View>
          )}

          {
            dataResult.map((item, index) => (
              <TouchableOpacity onPress={() => this.setState({ sort_select: item.key })} key={index} style={[{marginHorizontal:20,elevation:3,borderRadius:5,marginBottom:10,backgroundColor:'#fff',padding:10}]}>
                <Text style={[styles.textMedium,styles.normal,{color: colors.black,textAlign:'left',flex:1}]}>{item.author_name}</Text>
                <Text style={[styles.textNormal,styles.small_13,{color: colors.black,textAlign:'left',flex:1}]}>{item.content}</Text>
                <Text style={[styles.textNormal,styles.sub_small,{color: colors.gray,textAlign:'left',flex:1}]}>{item.humanized_posted_at}</Text>
              </TouchableOpacity>
            ))
          }
          {this._renderFooter()}
          <View style={{ marginTop:15}}/>

          </ScrollView>

        </View>

      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  authenticatedUser: state.authenticatedUser.result,

  dataResult: state.comments_list.result,
  dataFetching: state.comments_list.isFetching,
  dataFetchingFirst: state.comments_list.isFetching_first,
  dataPage: state.comments_list.page,
  dataTotal: state.comments_list.total,

  loading: state.comments_post.isSubmitting,
  result: state.comments_post.submitResult,
});

export default connect(mapStateToProps,{
  getComments,getCommentsPage,submitCoomentPost
})(Home);

const _styles = {
  imageBackground: {
    ...styles.flex1,
    resizeMode: "strech"
  }
}
