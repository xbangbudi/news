import React, { Component } from 'react';
import { View, Text ,StatusBar,SafeAreaView,TouchableOpacity,ScrollView,Image,ActivityIndicator,Linking,RefreshControl,Modal,FlatList,Alert,Dimensions} from 'react-native';
import styles, { colors,itemWidthslider,itemWidthslidercontener } from '../styles/index.style';
import Input from '../Components/Input'
import AutoHeightImage from 'react-native-auto-height-image';
import Button from '../Components/Button'
import Icon from 'react-native-vector-icons/AntDesign';

const {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT,
} = Dimensions.get('window');
import { Formik } from 'formik'
import * as Yup from 'yup'

const schema = Yup.object().shape({
    name: Yup.string()
      .required("Please input your name"),
    password: Yup.string()
      .min(6, 'Password must be at least 6 character')
      .required("Please input your password"),
    c_password: Yup.string()
      .required("Please input your Password Confirmation")
      .oneOf([Yup.ref("password")], "Password doesn't match"),
    email: Yup.string()
      .email("Please input a valid email")
      .required("Please input your email")

})
//redux
import { connect } from 'react-redux';
import { submitRegister } from '../../actions/register';
import { setAuthenticatedUser } from '../../actions/_authenticatedUser';

class Home extends Component {


  handleSubmit = (values, actions) => {
    const data = {
      email : values.email,
      password : values.password,
      name : values.name,
      c_password : values.c_password
    }
    const { submitRegister } = this.props;
    submitRegister(data)
      .then(() => {
        const { result,setAuthenticatedUser } = this.props;
        var datas = {
          user:result.data,
          token:result.meta.access_token,
        }
        setAuthenticatedUser(datas)
        this.props.navigation.popToTop()
    });
  }


  renderForm = (props,loading) => {
    const { values,errors,data } = props

    return (
      <View style={{}}>
      <Text style={[styles.textBold,styles.normal,{color: colors.black,textAlign:'left',marginHorizontal:0,marginBottom:5}]}>Name</Text>
      <Input
        placeholder={'Name'}
        style_input={{marginBottom:10}}
        onChangeText={props.handleChange('name')}
        value={props.values.name}
        autoCapitalize="none"
      />
      {errors.name && (
        <Text style={[styles.textBold,styles.small_13,{color: colors.red,textAlign:'left',marginBottom:10}]}>{errors.name}</Text>
      )}

      <Text style={[styles.textBold,styles.normal,{color: colors.black,textAlign:'left',marginHorizontal:0,marginBottom:5}]}>Email</Text>
      <Input
        placeholder={'Email'}
        style_input={{marginBottom:10}}
        onChangeText={props.handleChange('email')}
        value={props.values.email}
        keyboardType={'email-address'}
        autoCapitalize="none"
      />
      {errors.email && (
        <Text style={[styles.textBold,styles.small_13,{color: colors.red,textAlign:'left',marginBottom:10}]}>{errors.email}</Text>
      )}

      <Text style={[styles.textBold,styles.normal,{color: colors.black,textAlign:'left',marginHorizontal:0,marginBottom:5,}]}>Password</Text>
      <Input
        placeholder={'Password'}
        style_input={{marginBottom:10}}
        onChangeText={props.handleChange('password')}
        value={props.values.password}
        secureTextEntry
        autoCapitalize="none"
      />
      {errors.password && (
        <Text style={[styles.textBold,styles.small_13,{color: colors.red,textAlign:'left',marginBottom:10}]}>{errors.password}</Text>
      )}

      <Text style={[styles.textBold,styles.normal,{color: colors.black,textAlign:'left',marginHorizontal:0,marginBottom:5,}]}>Confirmation Password</Text>
      <Input
        placeholder={'Confirmation Password'}
        style_input={{marginBottom:10}}
        onChangeText={props.handleChange('c_password')}
        value={props.values.c_password}
        secureTextEntry
        autoCapitalize="none"
      />
      {errors.c_password && (
        <Text style={[styles.textBold,styles.small_13,{color: colors.red,textAlign:'left',marginBottom:10}]}>{errors.c_password}</Text>
      )}



      <Button onPress={(e) => props.handleSubmit(e)} name={'Submit'} style={{marginVertical:10}} loading={loading}/>

      </View>
    )
  }

  render() {
    const { authenticatedUser,loading } = this.props;
    const initialValues = {
      name:'',
      email:'',
      password:'',
      c_password:''
    }
    return (
      <SafeAreaView style={styles.flex1}>
      <StatusBar translucent={true} backgroundColor={'transparent'} barStyle={"dark-content"}/>
        <View style={{flex:1}}>
          <View style={[styles.view_header_not_shadow_stuck_shadow,styles.flexRow,{backgroundColor:colors.white,paddingHorizontal:20}]}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{ flex:1,justifyContent: 'center',alignItems: 'center'}}>
              <Icon name="left" size={20} color="#000" />
            </TouchableOpacity>
            <View style={{ flex:8,justifyContent: 'center',alignItems: 'center'}}>
              <Text style={[styles.textMedium,styles.normal_up,{color: colors.black,textAlign:'left'}]}>Registration</Text>
            </View>
            <View style={{ flex:1}}/>
          </View>



          <ScrollView style={{flex:1,paddingHorizontal:20,paddingVertical:10}}>



          <Formik
            onSubmit={this.handleSubmit.bind(this)}
            validationSchema={schema}
            initialValues={initialValues}
            validateOnChange={false}
            innerRef={p => (this.formik = p)}
            >
            {(props) => this.renderForm(props,loading)}
          </Formik>


          </ScrollView>




        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.register.isSubmitting,
  result: state.register.submitResult,
});

export default connect(mapStateToProps,{submitRegister,setAuthenticatedUser})(Home);

const _styles = {
  imageBackground: {
    ...styles.flex1,
    resizeMode: "strech"
  }
}
