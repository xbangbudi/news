import React, { Component } from 'react';
import { View, Text ,StatusBar,SafeAreaView,TouchableOpacity,Image,ActivityIndicator,ScrollView,Modal,ImageBackground} from 'react-native';
import styles, { colors } from '../styles/index.style';
import Icon from 'react-native-vector-icons/AntDesign';
import Input from '../Components/Input'
import Button from '../Components/Button'
import Toast from 'react-native-toast-message'
//redux
import { connect } from 'react-redux';
import { submitLogin } from '../../actions/auth';
import { setAuthenticatedUser } from '../../actions/_authenticatedUser';
//end
import { Formik } from 'formik'
import * as Yup from 'yup'

const schema = Yup.object().shape({
    password: Yup.string()
      .min(6, 'Password must be at least 6 character')
      .required("Please input your password"),
    email: Yup.string()
      .email("Please input a valid email")
      .required("Please input your email")

})

class Home extends Component {

  handleSubmit = (values, actions) => {
    const data = {
      email : values.email,
      password : values.password
    }
    const { submitLogin } = this.props;
    submitLogin(data)
      .then(() => {
        const { result,setAuthenticatedUser } = this.props;
        var datas = {
          user:result.data,
          token:result.meta.access_token,
        }
        setAuthenticatedUser(datas)
        this.props.navigation.goBack()
    });
  }

  toas(message= '',type = 'error'){
    Toast.show({
      type: type,
      visibilityTime: 2000,
      position: 'bottom',
      text1: 'HYPECLAN',
      text2: message+' 👋'
    })
  }

  renderForm = (props,loading) => {
    const { values,errors,data } = props

    return (
      <View style={{}}>

      <Text style={[styles.textBold,styles.normal,{color: colors.black,textAlign:'left',marginHorizontal:0,marginBottom:5}]}>Email</Text>

      <Input
        placeholder={'Email'}
        style_input={{marginBottom:10}}
        onChangeText={props.handleChange('email')}
        value={props.values.email}
        keyboardType={'email-address'}
        autoCapitalize="none"
      />
      {errors.email && (
        <Text style={[styles.textBold,styles.small_13,{color: colors.red,textAlign:'left',marginBottom:10}]}>{errors.email}</Text>
      )}

      <Text style={[styles.textBold,styles.normal,{color: colors.black,textAlign:'left',marginHorizontal:0,marginBottom:5,}]}>Password</Text>

      <Input
        placeholder={'Password'}
        style_input={{marginBottom:10}}
        onChangeText={props.handleChange('password')}
        value={props.values.password}
        secureTextEntry
        autoCapitalize="none"
      />
      {errors.password && (
        <Text style={[styles.textBold,styles.small_13,{color: colors.red,textAlign:'left',marginBottom:10}]}>{errors.password}</Text>
      )}



      <Button onPress={(e) => props.handleSubmit(e)} name={'Login'} style={{marginVertical:10}} loading={loading}/>

      </View>
    )
  }


  render() {
    const { loading } = this.props;
    const initialValues = {
      email:'',
      password:''
    }
    return (
      <SafeAreaView style={[styles.flex1,{backgroundColor:colors.white}]}>
        <StatusBar translucent={true} backgroundColor={'transparent'} barStyle={'dark-content'}/>

        <View style={{flex:1}}>

        <View style={[styles.view_header,styles.flexRow,{backgroundColor:colors.white,paddingHorizontal:20}]}>

          <View style={{ flex:8,alignItems: 'center',justifyContent: 'center'}}>
            <Text style={{color: '#000',fontFamily:'Montserrat-Regular',fontSize: 15,textAlign:'center'}}></Text>
          </View>
          <View style={{ flex:1}}/>
        </View>

          <ScrollView style={{flex:1}}>

              <View style={[styles.center,{marginVertical:20}]}>
                <Image
                  style={{height:100,width:'100%',marginTop:20}}
                  source={require('../img/app/logo-universitas-potensi-utama-1024x1024.jpg')}
                  resizeMode='contain'
                />
              </View>

              <View style={{marginHorizontal:15,shadowColor: '#969696',shadowOffset: { width: 0, height: 2 },shadowOpacity: 0.5,shadowRadius: 2,elevation:3,backgroundColor:'#fff',padding:15,borderRadius:10,marginBottom:20}}>

                <Text style={[styles.textBold,styles.large,{color: colors.black,textAlign:'center',marginHorizontal:20,marginBottom:10}]}>Member Login</Text>
                <Text style={[styles.textNormal,styles.small,{color: colors.gray,textAlign:'center',marginHorizontal:20,marginBottom:20}]}>Please Login to continue</Text>

                <Formik
                onSubmit={this.handleSubmit.bind(this)}
                validationSchema={schema}
                initialValues={initialValues}
                validateOnChange={false}
                innerRef={p => (this.formik = p)}
                >
                {(props) => this.renderForm(props,loading)}
              </Formik>
              <Button onPress={() => this.props.navigation.navigate('Register')} name={'Register'} style={{marginVertical:10}} />

              </View>

          </ScrollView>
        </View>



      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.auth.isSubmitting,
  result: state.auth.submitResult,
});

export default connect(mapStateToProps,{
  submitLogin,setAuthenticatedUser
})(Home);

const _styles = {
  imageBackground: {
    ...styles.flex1,
    resizeMode: "strech"
  }
}
