import React from 'react'
import { useEffect } from 'react'
import { StyleSheet, Text, View, FlatList, StatusBar, SafeAreaView, TouchableOpacity } from 'react-native'
import { useDispatch, useSelector } from 'react-redux';
import { getCategory } from '../../actions/category';
import { DEFAULT_BACKGROUND, MAIN_COLOR, SECONDARY_COLOR, TEXT } from '../../constants/colors';
import { camelize } from '../../constants/func';

const Category = ({ navigation }) => {
    const dispatch = useDispatch();
    const { result } = useSelector(state => state.category)
    useEffect(() => {
        dispatch(getCategory());
    }, [])

    const onPress = (data) => {
        navigation.navigate('NewsCategory', {
            data: data,
        })
    }

    const renderItem = ({ item }) => (
        <TouchableOpacity
            onPress={() => onPress(item)}
            style={styles.item}
        >
            <Text style={styles.title}>{camelize(item.category_name)}</Text>
        </TouchableOpacity>
    );
    return (
        <SafeAreaView style={styles.container}>
            <Text style={{ fontSize: 22, textAlign: 'center', fontWeight: 'bold', marginVertical: 10, color: TEXT }}>Kategori Berita</Text>
            <FlatList
                data={result}
                renderItem={renderItem}
                keyExtractor={item => item.category_id}
            />
        </SafeAreaView>
    )
}

export default Category

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
    },
    item: {
        backgroundColor: DEFAULT_BACKGROUND,
        padding: 15,
        marginVertical: 5,
        marginHorizontal: 16,
        borderRadius: 5,
        borderColor: SECONDARY_COLOR,
        borderWidth: 2
    },
    title: {
        fontSize: 18,
        color: TEXT
    },
})
