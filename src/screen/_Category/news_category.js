import React from 'react'
import { useEffect } from 'react';
import { View, Text ,StatusBar,SafeAreaView,TouchableOpacity,ScrollView,Image,ActivityIndicator,Linking,RefreshControl,FlatList,Alert,ImageBackground,Dimensions} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import styles, { colors,itemWidthslider,itemWidthslidercontener } from '../styles/index.style';
import { getPostCategory } from '../../actions/posts';
import { useDispatch, useSelector } from 'react-redux';

import moment from 'moment';
import 'moment/locale/id'
moment.locale('id')

const {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT,
} = Dimensions.get('window');

const NewsCategory = ({ navigation, route }) => {
  const { data } = route.params;
  const { category_id } = data;
  const dispatch = useDispatch();
  const { isFetching_first, isFetching, result_category } = useSelector(state => state.posts)

  useEffect(() => {
    dispatch(getPostCategory(category_id))
  }, [])

  const onPressProductDetail = (data) => {
    navigation.navigate('NewsDetail', {
      data: data,
    })
  }

  const clea_taghtml = (data) => {
    const regex = /(<([^>]+)>)/ig;
    const result = data.replace(regex, '');
    return result;
  }

  const renderItem = (item) => {
    return (
      <TouchableOpacity onPress={() => onPressProductDetail(item)} style={[styles.flexRow, { marginHorizontal: 15, borderBottomWidth: 0.5, borderColor: '#969696', paddingVertical: 15 }]}>
        <View style={{ flex: 3, justifyContent: 'center' }}>
          <Image
            style={{ height: 70, width: '100%', borderRadius: 5 }}
            source={{ uri: item.thumb_thumbnail_url }}
            resizeMode='contain'
          />
        </View>
        <View style={{ flex: 7, justifyContent: 'center', marginLeft: 10 }}>
          <Text style={[styles.textMedium, styles.mini, { color: colors.red, textAlign: 'left' }]}>{moment(item.posted_at).format('dddd, DD MMMM YYYY')}</Text>
          <Text style={[styles.textBold, styles.small, { color: colors.black, textAlign: 'left' }]}>{item.title}</Text>
          <Text style={[styles.textNormal, styles.sub_small, { color: colors.gray_, textAlign: 'left' }]} numberOfLines={2}>{clea_taghtml(item.content)}</Text>
        </View>
      </TouchableOpacity>
    )
  }

  return (
    <SafeAreaView style={styles.flex1}>
      <StatusBar translucent={true} backgroundColor={'transparent'} barStyle={"dark-content"} />
      <View style={{ flex: 1 }}>
        <View style={[styles.view_header_not_shadow_stuck_shadow, styles.flexRow, { backgroundColor: colors.white, paddingHorizontal: 20 }]}>
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Image
              style={{ height: 20, width: '100%' }}
              source={require('../img/app/logo-universitas-potensi-utama-1024x1024.jpg')}
              resizeMode='contain'
            />
          </View>
          <View style={{ flex: 8, justifyContent: 'center' }}>
            <Text style={[styles.textMedium, styles.normal_up, { color: colors.black, textAlign: 'left' }]}>UPU MOBILE</Text>
          </View>
          <TouchableOpacity onPress={() => {
            Linking.openURL('https://seodimedan.com')
          }} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Icon name="globe" size={20} color="#000" />
          </TouchableOpacity>
        </View>
        {isFetching_first ? (
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <ActivityIndicator size="large" color={colors.red} />
          </View>
        ) : (
          <FlatList
            data={result_category}
            renderItem={({ item }) => (
              renderItem(item)
            )}
            contentContainerStyle={{ paddingBottom: 70 }}
            ListEmptyComponent={
              <View style={[styles.center, { flex: 1 }]}>

                <Text style={[styles.textBold, styles.small, { color: colors.red, textAlign: 'left', marginTop: SCREEN_HEIGHT / 2 - 70 }]}>Artikel Tidak tersedia</Text>
                <Text style={[styles.textBold, styles.small, { color: colors.black, textAlign: 'left' }]}>Silahkan coba beberapa saat lagi</Text>
              </View>

            }
            keyExtractor={(item, index) => '_' + index}
            ListFooterComponent={
              <ActivityIndicator
                style={[
                  { paddingTop: 15 },
                  !isFetching && { display: 'none' },
                ]}
                size="large"
                color={colors.red}
              />
            }
            maxToRenderPerBatch={10}
            windowSize={2}
            updateCellsBatchingPeriod={10}
            initialNumToRender={6}
            onEndReachedThreshold={1}
          />
        )}


      </View>

    </SafeAreaView>
  )
}

export default NewsCategory

