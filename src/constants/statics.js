export const PRINTER_CONNECTION = ['Bluetooth', 'USB (OTG)', 'Wifi / LAN', 'Tidak Ada Printer'];
export const PRINTER_PAPER_SIZE = ['Printer Paper 58mm', 'Printer Paper 80mm'];
export const PRINT_CHARACTER = [32, 48];
export const PRINT_SEPARATOR = ['--------------------------------', '------------------------------------------------'];


export const NOMINAL_CHOICE = [50000, 100000];

const GOPAY_ICON = require('../assets/icon/payment/go-pay.png');
const OVO_ICON = require('../assets/icon/payment/ovo.png');
const DANA_ICON = require('../assets/icon/payment/dana.png');
const LINK_AJA_ICON = require('../assets/icon/payment/linkaja.png');
const AKULAKU_ICON = require('../assets/icon/payment/akulaku.png');
const KREDIVO_ICON = require('../assets/icon/payment/kredivo.png');

export const CARD_PAYMENT_METHOD = [
  { name: 'Debit Card', column: 'debitCard' },
  { name: 'Credit Card', column: 'creditCard' },
];
export const E_MONEY_PAYMENT_METHOD = [
  { name: 'Go-Pay', icon: GOPAY_ICON, column: 'goPay' },
  { name: 'OVO', icon: OVO_ICON, column: 'ovo' },
  { name: 'Dana', icon: DANA_ICON, column: 'dana' },
  { name: 'Link Aja', icon: LINK_AJA_ICON, column: 'linkAja' },
  { name: 'Akulaku', icon: AKULAKU_ICON, column: 'akulaku' },
  { name: 'Kredivo', icon: KREDIVO_ICON, column: 'kredivo' },
];


/* export const CASHLEZ_PAYMENT_ICON = {
  cash: cashlezCashIcon,
  card: cashlezCardIcon,
  ovo: cashlezOvoIcon,
  linkAja: cashlezLinkAjaIcon,
  mandiriPay: cashlezMandiriPayIcon,
};

export const REFUND_PAYMENT_METHOD = [
  { label: 'Refund with Cash', value: 'cash' },
  { label: 'Refund with Bank', value: 'bank' },
]; */


export const CUSTOMER_GENDER = ['Laki-Laki', 'Perempuan'];

export const REFUND_REASONS = [
  { name: 'Barang rusak / cacat' },
  { name: 'Pesanan telah di-cancel' },
  { name: 'Harga tidak sesuai' },
];
