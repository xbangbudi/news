export const MAIN_COLOR = '#f89a26';
export const SECONDARY_COLOR = '#eaeaeb';

export const BACKGROUND = '#ffffff';
export const DEFAULT_BACKGROUND = '#eceff1'; // e9e9ef

export const STATUS_BAR = '#f5882b';
export const NAV_BAR = MAIN_COLOR;
export const SEARCH_BAR = '#eceff1';
export const TAB_BAR = '#ffffff';
export const TAB_BAR_INDICATOR = MAIN_COLOR;
export const BOTTOM_TAB_BAR = 'ghostwhite';


export const TEXT = '#717a84';
export const SECONDARY_TEXT = '#b5bac2';
export const STRONG_TEXT = '#343434';
export const TITLE = '#343434';
export const LINE = '#cdcdcd';
export const SUBMIT_BUTTON = MAIN_COLOR;

export const LABEL_DEFAULT = '#343434';
export const LABEL_RED = '#f44336';
export const LABEL_GREEN = '#21ba45';
export const LABEL_YELLOW = '#FDD835';
export const LABEL_BLUE = '#0984E3';
export const LABEL_ORANGE = '#f08d35';


export const FACEBOOK = '#405A93';
export const GOOGLE = '#ffffff';
