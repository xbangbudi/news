export const APP_ENV = 'dev';
export const APP_NAME = 'HypeClan';
export const APP_KEY = 'HypeClan';

export const APP_URL = 'https://seodimedan.com';

export const VERSION_APP = {
  ios: {
    string:'1.0.10',
    code:1010,
  },
  android: {
    string:'1.0.10',
    code:1010,
  }
}


export const API_URL = `${APP_URL}/api/v1`;
export const API_HEADER = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
  dataType: 'json',
  'X-Requested-With': 'XMLHttpRequest',
};
export const API_HEADER_MULTIPART = {
  Accept: 'application/json',
  'Content-Type': 'multipart/form-data',
  dataType: 'json',
  'X-Requested-With': 'XMLHttpRequest',
};

export const API_RESPONSE = {
  SUCCESS: 'success',
  FAILED: 'failed',
};


export const BUTTON_DEBOUNCE_TIME = 700;
