import * as env from '../constants/env';

const url = env.API_URL;
const header = env.API_HEADER;

export const about = () => {
	return fetch(`${url}/about`, {
    method: 'GET',
    headers: header,
  }).then(response => response.json())
};

export const links = () => {
	return fetch(`${url}/link`, {
    method: 'GET',
    headers: header,
  }).then(response => response.json())
};

export const send_message = (token,formdata) => {
	header.Authorization = 'Bearer '+token;
	return fetch(`${url}/messages`, {
    method: 'POST',
		headers: header,
    body: formdata,
  }).then(response => response.json())
};

export const message_detail = (token,id) => {
	header.Authorization = 'Bearer '+token;
	return fetch(`${url}/messages/${id}`, {
    method: 'GET',
		headers: header,
  }).then(response => response.json())
};

export const industri = () => {
	return fetch(`${url}/industry`, {
    method: 'GET',
		headers: header,
  }).then(response => response.json())
};

export const promo = () => {
	return fetch(`${url}/promo`, {
    method: 'GET',
		headers: header,
  }).then(response => response.json())
};
