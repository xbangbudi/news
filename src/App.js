import React from 'react';
// import { NavigationContainer } from '@react-navigation/native';
import Navigation from './Navigation'
import {Provider} from 'react-redux'
import configureStore from './store';

const store = configureStore({});
import Toast from 'react-native-toast-message'

const App = () => {
  return (
    <Provider store={store}>
      <Navigation store={store}/>
      <Toast ref={(ref) => Toast.setRef(ref)} />
    </Provider>
  );
};

export default App;
